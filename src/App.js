//  breakout room with judith, derek, jamal, dessance, caity

import React,{ useState } from "react";
import todosList from "./todos.json";

function App () {
  const [todos,setTodos] = useState(todosList)
  const [input,setInput] = useState("")
  
  function addItem (event) {
    if(event.key === "Enter"){
      event.preventDefault()
      const newItem = {
      userId: 1,
      id: (todos.length + 1),
      title: `${event.target.value}`,
      completed: false
    }
    
    setTodos([...todos, newItem])
    console.log(newItem)
    event.target.value = ""
    // setInput("")
  }

}
  function TodoList ({todos,toggleItem,subtractItem}) {
    return (
      <section className="main">
        <ul className="todo-list">
          {todos.map((todo) => (
            <TodoItem
            key={todo.title}
            toggleItem = {toggleItem}
            subtractItem ={subtractItem}
            todo={todo} />
          ))}
        </ul>
      </section>
    );
}

function TodoItem ({todo, toggleItem, subtractItem}) {
      const {
        completed,
        id,
        title
      } = todo
  return (
    <li className={completed ? "completed" : ""}>
      <div className="view">
        <input className="toggle"
         type="checkbox"
         checked={completed}
         onClick = {() => toggleItem(id)}/>
        <label>{title}</label>
        <button className="destroy" onClick = {() => subtractItem(id)} />
      </div>
    </li>
  );
}
function toggleItem (id) {
  const newToDos = todos.map((todo)=> {
    if(id === todo.id){
      return {...todo, completed: !todo.completed}
      }
    return todo
  })
    setTodos(newToDos)
}
function subtractItem (id) {
  const newToDos = todos.filter((todo)=> {
   const keptToDos = id !== todo.id
   return keptToDos
  })
    setTodos(newToDos)
}
function removeCompleted () {
  const newToDos = todos.filter((todo) => {
    const keptToDos =  !todo.completed
    return keptToDos
  })
    setTodos(newToDos)
}
  // useState = {
  //   todos: todosList,
  // };
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
          className="new-todo"
          placeholder="What needs to be done?"
          autoFocus
           onChange = {(event) => setInput(event.target.value)}
           onKeyDown = {addItem}
           />
        </header>
        <TodoList todos={todos}
        toggleItem = {toggleItem}
        subtractItem = {subtractItem}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button
          className="clear-completed"
          onClick = {() => removeCompleted()}
          >Clear completed</button>
        </footer>
      </section>
    );
}
export default App;